# Changes in 0.2.0

* Documentation updtes.

# Changes in 0.2.0-alpha.1

* Update coap-message to 0.3.0-alpha.1
  (the "fallible write operations" release).

* Make request extraction and response creation fallible.

  Upgrading applications may compatibly use the Infallible type as the error
  and carry errors such as the presence of unknown critical options in the success variant.
  As all fallible steps of creating a response provide a suitable union error type,
  this helps with ergonomics.

* Undeprecate the implementation of coap-handler for Option.

  This is something that can not be added downstream,
  it's handy
  and does not cause much of maintenance load.

* Remove all deprecated items.
  They are found in `coap-handler-implementations` instead.
