[![Build Status](https://gitlab.com/chrysn/coap-handler/badges/master/pipeline.svg)](https://gitlab.com/chrysn/coap-handler/commits/master)
![Maintenance](https://img.shields.io/badge/maintenance-activly--developed-brightgreen.svg)

# coap-handler

The `coap-handler` crate defines an interface between a [CoAP] server (that listens for
requests on the network and parses the messages) and request handlers (that process the
requests and creates responses from them).

The interface is generic over message formats by using the [coap-message] crate, which allows
the handler to construct the response right into the send buffer prepared by the server
implementation. By separating the request processing and the response phase, a server can be
implemented even on network stacks that have only a single network buffer.

 Convenience, example and reference implementations are available in the [coap-handler-implementations] crate.

[CoAP]: https://coap.technology/
[coap-message]: https://crates.io/crates/coap-message
[coap-handler-implementations]: https://crates.io/crates/coap-handler-implementations

Known shortcomings of the current interface are:

* No consideration for asynchronous processing.
* Handler mutability is a bit iffy -- there's no way yet for the server to express any promise
  about only running one handler at a time, thus handlers often hold shared references to a
  [`RefCell`] that is [`borrow_mut()`]'d (if no other code that can be concurrent with the CoAP
  server can have access to the `T`), or [`try_borrow_mut()`]'d (and errs back with a 5.03
  Max-Age:0 response).

  Alternatives (where multiple handlers could be built based on a single mutable reference to
  their data) are being explored.
* Multiple responses (as, for example, in observations) are not supported.

[`RefCell`]: https://doc.rust-lang.org/core/cell/struct.RefCell.html
[`borrow_mut()`]: https://doc.rust-lang.org/core/cell/struct.RefCell.html#method.borrow_mut
[`try_borrow_mut()`]: https://doc.rust-lang.org/core/cell/struct.RefCell.html#method.try_borrow_mut


The main item of this crate is the [Handler] trait. The [Reporting] trait can be implemented in
addition to [Handler] to ease inclusion of the rendered resource in discovery, eg. through
`/.well-known/core` as implemented in [coap-handler-implementations].

License: MIT OR Apache-2.0
